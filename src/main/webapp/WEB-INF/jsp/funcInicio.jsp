<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <h1>Funcionarios</h1>
    <h3><a href="/Cafeteria/func/cadastrar">Cadastrar </a></h3>
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nome: </th>
            <th>CPF: </th>
            <th>Permissão: </th>
            <th>Setor: </th>
            <th>Opções</th>
        </tr>
        </thead>

        <tbody>
        <c:forEach var="f" items="${funcionarios}">
            <tr>
                <th>${f.nomfun}</th>
                <th>${f.cpffun}</th>
                <th>${f.permissao.nome}</th>
                <th>${f.setor.nomsetor}</th>
                <th><a href="/Cafeteria/func/editar?codfun=${f.codfun}">Editar </a><p> </p>
                    <a href="/Cafeteria/func/excluir?codfun=${f.codfun}"> Excluir</a></th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <h5><a href="/Cafeteria/admin/inicio">Voltar!</a></h5>
</div>
</body>
</html>
