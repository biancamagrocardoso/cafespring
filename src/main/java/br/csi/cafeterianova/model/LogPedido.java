package br.csi.cafeterianova.model;

public class LogPedido {
    private int codped;
    private Funcionario funcionario;
    private Cliente cliente;
    private int mesa;
    private String descricao;

    public int getCodped() {
        return codped;
    }

    public void setCodped(int codped) {
        this.codped = codped;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
