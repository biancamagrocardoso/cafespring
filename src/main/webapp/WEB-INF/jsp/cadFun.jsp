<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <form:form method="post" action="/Cafeteria/func/cadastrar" modelAttribute="func">
        <form:label path="nomfun">Nome: </form:label>
        <form:input path="nomfun" type="text" />

        <form:label path="cpffun">CPF: </form:label>
        <form:input path="cpffun" type="text" />

        <form:label path="senha">Senha: </form:label>
        <form:input path="senha" type="password" />

        <form:label path="permissao.id">Permissão: </form:label>
        <form:select path="permissao.id">
            <c:forEach var="p" items="${perms}">
                <form:option value="${p.id}">${p.nome}</form:option>
            </c:forEach>
        </form:select>

        <form:label path="setor.codsetor">Setor: </form:label>
        <form:select path="setor.codsetor">
            <c:forEach var="s" items="${sets}">
                <form:option value="${s.codsetor}">${s.nomsetor}</form:option>
            </c:forEach>
        </form:select>
        <input type="submit" value="Cadastrar">
    </form:form>
    <h5><a href="/Cafeteria/func/inicio">Voltar</a></h5>
</div>
</body>
</html>
