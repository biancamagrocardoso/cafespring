package br.csi.cafeterianova;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CafeteriaNovaApplication {

    public static void main(String[] args) {
        SpringApplication.run(CafeteriaNovaApplication.class, args);
    }

}
