<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Garçom</title>
</head>
<body>
<div class="container">

    <form:form method="get" action="/Cafeteria/garcom/comanda" modelAttribute="login">
        <form:label path="cpf">Mesa: </form:label>
        <form:input path="cpf" type="text"/>

        <input type="submit" value="Prosseguir">
    </form:form>
    <h3><a href="/Cafeteria/admin/sair">Sair</a></h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nome: </th>
            <th>Mesa: </th>
            <th>Descrição: </th>
            <th>Opção:</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="p" items="${peds}">
            <tr>
                <th>${p.nome}</th>
                <th>${p.mesa}</th>
                <th>${p.descricao}</th>
                <th><a href="/Cafeteria/garcom/comanda?cpf=${p.mesa}">Editar!</a></th>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>
</body>
</html>
