package br.csi.cafeterianova.dao;

import br.csi.cafeterianova.model.Setor;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SetorDao {
    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public Setor getSet(int codsetor) {
        Setor set = new Setor();
        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " SELECT * FROM setor where codsetor = ?";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, codsetor);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                set.setCodsetor(resultSet.getInt("codsetor"));
                set.setNomsetor(resultSet.getString("nomsetor"));
            }

        } catch (PSQLException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return set;
    }

    public ArrayList<Setor> listSet(){
        ArrayList<Setor> setors = new ArrayList<Setor>();

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = " SELECT * FROM setor";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                Setor set = new Setor();
                set.setCodsetor(resultSet.getInt("codsetor"));
                set.setNomsetor(resultSet.getString("nomsetor"));
                setors.add(set);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return setors;
    }

    public void setSet(Setor set){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "INSERT INTO setor (nomsetor) VALUES (?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, set.getNomsetor());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updSet (Setor set){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "UPDATE setor SET nomsetor = ? WHERE codsetor = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, set.getNomsetor());
            preparedStatement.setInt(2, set.getCodsetor());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void delSet(int codsetor){
        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " DELETE FROM setor where codsetor = ?";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, codsetor);
            this.preparedStatement.executeQuery();

        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
