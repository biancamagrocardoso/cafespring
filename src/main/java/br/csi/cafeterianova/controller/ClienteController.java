package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.ClienteDao;
import br.csi.cafeterianova.model.Cliente;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/cli")
public class ClienteController {

    @GetMapping("/inicio")
    public String inicio(Model model){
        model.addAttribute("clientes", new ClienteDao().listCli());

        return "cliInicio";
    }

    @GetMapping("/cadastrar")
    public String cadastro(Model model){

        model.addAttribute("cli", new Cliente());

        return "cadCli";
    }

    @PostMapping("/cadastrar")
    public RedirectView cadCli(@ModelAttribute Cliente cli){
        RedirectView redirect = new RedirectView("/Cafeteria/cli/inicio");

        new ClienteDao().setCliente(cli);

        return redirect;
    }

    @GetMapping("/editar")
    public String editar(@RequestParam int codcli, Model model){

        Cliente cli = new ClienteDao().getCli(codcli);

        model.addAttribute("cliente", cli);

        model.addAttribute("cli", new Cliente());

        return "updCli";
    }

    @PostMapping("/editar")
    public RedirectView updCli(@ModelAttribute Cliente cli){
        RedirectView redirect = new RedirectView("/Cafeteria/cli/inicio");

        new ClienteDao().updCli(cli);

        return redirect;
    }

    @GetMapping("/excluir")
    public RedirectView delete(@RequestParam int codcli){
        RedirectView redirect = new RedirectView("/Cafeteria/cli/inicio");

        new ClienteDao().delCli(codcli);

        return redirect;
    }
}