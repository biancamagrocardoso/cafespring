package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.FuncionarioDao;
import br.csi.cafeterianova.dao.PermissaoDao;
import br.csi.cafeterianova.dao.SetorDao;
import br.csi.cafeterianova.model.Funcionario;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/func")
public class FuncionarioController {

    @GetMapping("/inicio")
    public String inicio(Model model){
        model.addAttribute("funcionarios", new FuncionarioDao().listFun());

        return "funcInicio";
    }

    @GetMapping("/cadastrar")
    public String cadFun(Model model){
        model.addAttribute("perms", new PermissaoDao().listPerm());
        model.addAttribute("sets", new SetorDao().listSet());
        model.addAttribute("func", new Funcionario());

        return "cadFun";
    }

    @PostMapping("/cadastrar")
    public RedirectView cadastrar(@ModelAttribute("func") Funcionario func, Model model){
        RedirectView redirect = new RedirectView("/Cafeteria/func/inicio");

        new FuncionarioDao().setFuncionario(func);

        return redirect;
    }

    @GetMapping("/editar")
    public String update(@RequestParam int codfun, Model model){
        System.out.println(codfun);
        Funcionario func = new FuncionarioDao().getFuncionario(codfun);

        model.addAttribute("func", func);

        model.addAttribute("funcionario", new Funcionario());

        model.addAttribute("perms", new PermissaoDao().listPerm());

        model.addAttribute("sets", new SetorDao().listSet());

        return "updFun";
    }

    @PostMapping("/editar")
    public RedirectView editar(@ModelAttribute Funcionario func){
        RedirectView redirect = new RedirectView("/Cafeteria/func/inicio");

        int perm = func.getPermissao().getId();

        new FuncionarioDao().updFun(func);

        return redirect;
    }

    @GetMapping("/excluir")
    public RedirectView delete(@RequestParam int codfun){
        new FuncionarioDao().delfun(codfun);

        RedirectView redirect = new RedirectView("/Cafeteria/func/inicio");

        return redirect;
    }
}