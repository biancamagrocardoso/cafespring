<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <h1>Olá ${usuario_logado.nomfun}</h1>

    <ul>
        <li>
            <a href="/Cafeteria/admin/funcionarios">Funcionarios</a>
        </li>
        <li>
            <a href="/Cafeteria/admin/clientes">Clientes</a>
        </li>
        <li>
            <a href="/Cafeteria/admin/setor">Setores</a>
        </li>
        <li>
            <a href="/Cafeteria/admin/pedidos">Pedidos</a>
        </li>
    </ul>

    <h5><a href="/Cafeteria/admin/sair">Sair</a></h5>

</div>
</body>
</html>
