<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <h3><a href="/Cafeteria/admin/inicio">Voltar!</a></h3>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Cliente: </th>
            <th>Mesa: </th>
            <th>Descrição: </th>
            <th>Funcionario:</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="p" items="${pedidos}">
            <tr>
                <th>${p.cliente.nomcli}</th>
                <th>${p.mesa}</th>
                <th>${p.descricao}</th>
                <th>${p.funcionario.nomfun}</th>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
