package br.csi.cafeterianova.model;

public class Setor {
    private int codsetor;
    private String nomsetor;

    public Setor(int codsetor, String nomsetor) {
        this.codsetor = codsetor;
        this.nomsetor = nomsetor;
    }

    public Setor() {

    }

    public int getCodsetor() {
        return codsetor;
    }

    public void setCodsetor(int codsetor) {
        this.codsetor = codsetor;
    }

    public String getNomsetor() {
        return nomsetor;
    }

    public void setNomsetor(String nomsetor) {
        this.nomsetor = nomsetor;
    }
}