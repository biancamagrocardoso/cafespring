package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.ClienteDao;
import br.csi.cafeterianova.dao.LogPedDao;
import br.csi.cafeterianova.dao.PedidoDao;
import br.csi.cafeterianova.model.Cliente;
import br.csi.cafeterianova.model.LogPedido;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("caixa")
public class CaixaController {

    @GetMapping("inicio")
    public String inicio(Model model){

        model.addAttribute("peds", new PedidoDao().listPed());

        return "caixInicio";
    }

    @GetMapping("/pagar")
    public String pagar(@RequestParam int mesa, Model model){

        model.addAttribute("pedido", new LogPedido());
        model.addAttribute("ped", new PedidoDao().getPed(mesa));
        model.addAttribute("cli", new ClienteDao().listCli());
        model.addAttribute("mesa", mesa);

        return "pagar";
    }

    @PostMapping("/salvar")
    public RedirectView pagar(@ModelAttribute LogPedido ped, @RequestParam int mesa){

        System.out.println(mesa);

        new LogPedDao().setPedido(ped);
        new PedidoDao().delPed(mesa);

        return new RedirectView("/Cafeteria/caixa/inicio");
    }

    @GetMapping("/cadcli")
    public String cadcli(Model model){

        model.addAttribute("cli", new Cliente());

        return "cadcli2";
    }

    @PostMapping("/cadcli")
    public RedirectView cad(@ModelAttribute Cliente cli){

        new ClienteDao().setCliente(cli);

        return new RedirectView("/Cafeteria/caixa/inicio");
    }
}