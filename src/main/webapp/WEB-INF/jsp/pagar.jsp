<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <h1>Mesa: ${mesa}</h1>
    <h1>Cliente: ${ped.nome}</h1>
    <form:form action="/Cafeteria/caixa/salvar" method="post" modelAttribute="pedido">
        <form:input path="mesa" type="hidden" value="${mesa}" />
        <form:input path="funcionario.codfun" type="hidden" value="${ped.funcionario.codfun}" />

        <form:label path="cliente.codcli">Cliente</form:label>
        <form:select path="cliente.codcli">
            <c:forEach var="c" items="${cli}">
                <form:option value="${c.codcli}">${c.nomcli}</form:option>
            </c:forEach>
        </form:select>

        <form:label path="descricao">Descrição: </form:label>
        <form:input path="descricao" type="text" value="${ped.descricao}"/>

        <input type="submit" value="Salvar">
    </form:form>

    <h5><a href="/Cafeteria/caixa/cadcli">Cliente não cadastrado!</a> </h5>

    <h5><a href="/Cafeteria/caixa/inicio">Voltar!</a> </h5>
</div>
</body>
</html>
