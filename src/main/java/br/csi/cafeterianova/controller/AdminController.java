package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.LogPedDao;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin")
public class AdminController {
    @GetMapping("/inicio")
    public String inicio () {

        return "admin";
    }

    @GetMapping("/funcionarios")
    public RedirectView funcionarios(){
        RedirectView redirect = new RedirectView("/Cafeteria/func/inicio");

        return redirect;
    }

    @GetMapping("/clientes")
    public RedirectView clientes(){
        RedirectView redirect = new RedirectView("/Cafeteria/cli/inicio");

        return redirect;
    }

    @GetMapping("/setor")
    public RedirectView setor(){
        RedirectView redirect = new RedirectView("/Cafeteria/set/inicio");

        return redirect;
    }

    @GetMapping("/pedidos")
    public String pedidos(Model model){

        model.addAttribute("pedidos", new LogPedDao().listPed());

        return "pedidos";
    }

    @GetMapping("/sair")
    public RedirectView sair(HttpServletRequest req){
        RedirectView redirect = new RedirectView("/Cafeteria/login");
        req.getSession().invalidate();

        return redirect;
    }
}
