<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>
<div class="container">
    <form:form method="post" action="/Cafeteria/set/cadastrar" modelAttribute="set">
        <form:label path="nomsetor">Nome: </form:label>
        <form:input path="nomsetor" type="text" />
        <input type="submit" value="Cadastrar">
    </form:form>
    <h5><a href="/Cafeteria/set/inicio">Voltar</a></h5>
</div>
</body>
</html>