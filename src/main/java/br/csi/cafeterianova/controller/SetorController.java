package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.SetorDao;
import br.csi.cafeterianova.model.Setor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/set")
public class SetorController {

    @GetMapping("/inicio")
    public String inicio(Model model){

        model.addAttribute("sets", new SetorDao().listSet());

        return "setInicio";
    }

    @GetMapping("/cadastrar")
    public String cadSet (Model model){

        model.addAttribute("set", new Setor());

        return "cadSet";
    }

    @PostMapping("/cadastrar")
    public RedirectView cadsetor(@ModelAttribute Setor set){

        new SetorDao().setSet(set);

        return new RedirectView("/Cafeteria/set/inicio");
    }

    @GetMapping("/editar")
    public String updSet(@RequestParam int codsetor, Model model){

        model.addAttribute("setor", new SetorDao().getSet(codsetor));

        model.addAttribute("set", new Setor());

        return "updSet";
    }

    @PostMapping("/editar")
    public RedirectView ediSet(@ModelAttribute Setor set){

        new SetorDao().updSet(set);

        return new RedirectView("/Cafeteria/set/inicio");
    }

    @GetMapping("/excluir")
    public RedirectView delSet(@RequestParam int codsetor){

        new SetorDao().delSet(codsetor);

        return new RedirectView("/Cafeteria/set/inicio");
    }
}