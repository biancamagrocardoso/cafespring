package br.csi.cafeterianova.dao;

import br.csi.cafeterianova.model.Cliente;
import br.csi.cafeterianova.model.Funcionario;
import br.csi.cafeterianova.model.Pedido;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PedidoDao {
    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public Pedido getPed(int mesa) {
        Pedido ped = new Pedido();
        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " SELECT * FROM comanda where mesa = ?";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, mesa);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                ped.setNome(resultSet.getString("nome"));
                ped.setDescricao(resultSet.getString("descricao"));
                ped.setMesa(resultSet.getInt("mesa"));
                Funcionario func = new FuncionarioDao().getFuncionario(resultSet.getInt("codfun"));
                ped.setFuncionario(func);
            }

        } catch (PSQLException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }

        if (ped.getNome() == null){

            return new Pedido();
        }

        return ped;
    }

    public ArrayList<Pedido> listPed(){
        ArrayList<Pedido> pedidos = new ArrayList<Pedido>();

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = " SELECT * FROM comanda";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                Pedido ped = new Pedido();
                ped.setNome(resultSet.getString("nome"));
                ped.setDescricao(resultSet.getString("descricao"));
                ped.setMesa(resultSet.getInt("mesa"));
                Funcionario func = new FuncionarioDao().getFuncionario(resultSet.getInt("codfun"));
                ped.setFuncionario(func);
                pedidos.add(ped);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return pedidos;
    }

    public void setPedido(Pedido ped){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "INSERT INTO comanda (nome, descricao, mesa, codfun) VALUES (?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, ped.getNome());
            preparedStatement.setString(2, ped.getDescricao());
            preparedStatement.setInt(3, ped.getMesa());
            preparedStatement.setInt(4, ped.getFuncionario().getCodfun());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updPed(Pedido ped){
        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "update comanda set nome = ?, descricao = ? where mesa = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, ped.getNome());
            preparedStatement.setString(2, ped.getDescricao());
            preparedStatement.setInt(3, ped.getMesa());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }

    }

    public void delPed(int mesa){
        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = "DELETE FROM comanda where mesa = ?";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, mesa);
            this.preparedStatement.executeQuery();

        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}