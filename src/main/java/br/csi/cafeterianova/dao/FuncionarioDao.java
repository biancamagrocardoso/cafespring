package br.csi.cafeterianova.dao;

import br.csi.cafeterianova.model.Funcionario;
import br.csi.cafeterianova.model.Permissao;
import br.csi.cafeterianova.model.Setor;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class FuncionarioDao {
    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public Funcionario getFuncionario(int codfun) {
        Funcionario func = null;

        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " SELECT * FROM funcionario, permissao, setor where codfun = ? and funcionario.id_permissao = permissao.id_permissao and funcionario.codsetor = setor.codsetor";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, codfun);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                func = new Funcionario();
                func.setCodfun(resultSet.getInt("codfun"));
                func.setNomfun(resultSet.getString("nomfun"));
                func.setCpffun(resultSet.getString("cpffun"));
                func.setSenha(resultSet.getString("senha"));
                func.setSetor(new Setor(resultSet.getInt("codsetor"), resultSet.getString("nomsetor")));
                func.setPermissao(new Permissao(resultSet.getInt("id_permissao"), resultSet.getString("nome_permissao")));
            }

        } catch (PSQLException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return func;
    }

    public ArrayList<Funcionario> listFun(){
        ArrayList<Funcionario> funcionarios = new ArrayList<Funcionario>();

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = " SELECT * FROM funcionario, permissao, setor where funcionario.id_permissao = permissao.id_permissao and funcionario.codsetor = setor.codsetor";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                Funcionario func = new Funcionario();
                func.setCodfun(resultSet.getInt("codfun"));
                func.setNomfun(resultSet.getString("nomfun"));
                func.setCpffun(resultSet.getString("cpffun"));
                func.setSenha(resultSet.getString("senha"));
                func.setSetor(new Setor(resultSet.getInt("codsetor"), resultSet.getString("nomsetor")));
                func.setPermissao(new Permissao(resultSet.getInt("id_permissao"), resultSet.getString("nome_permissao")));
                funcionarios.add(func);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return funcionarios;
    }

    public void setFuncionario(Funcionario func){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "INSERT INTO funcionario (nomfun, cpffun, senha, id_permissao, codsetor) VALUES (?, ?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, func.getNomfun());
            preparedStatement.setString(2, func.getCpffun());
            preparedStatement.setString(3, func.getSenha());
            preparedStatement.setInt(4, func.getPermissao().getId());
            preparedStatement.setInt(5, func.getSetor().getCodsetor());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void updFun (Funcionario func){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "UPDATE funcionario SET nomfun = ?, cpffun = ?, senha = ?, id_permissao = ?, codsetor = ? WHERE codfun = ?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, func.getNomfun());
            preparedStatement.setString(2, func.getCpffun());
            preparedStatement.setString(3, func.getSenha());
            preparedStatement.setInt(4, func.getPermissao().getId());
            preparedStatement.setInt(5, func.getSetor().getCodsetor());
            preparedStatement.setInt(6, func.getCodfun());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void delfun(int codfun){
        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " DELETE FROM funcionario where codfun = ?";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setInt(1, codfun);
            this.preparedStatement.executeQuery();

        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
