package br.csi.cafeterianova.dao;

import br.csi.cafeterianova.model.Funcionario;
import br.csi.cafeterianova.model.Permissao;
import br.csi.cafeterianova.model.Setor;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDao {
    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public Funcionario getLogin(String cpf) {
        Funcionario func = null;

        try (Connection connection = new ConectaDB().getConexao()) {
            this.sql = " SELECT * FROM funcionario, permissao, setor where cpffun = ? and funcionario.id_permissao = permissao.id_permissao and funcionario.codsetor = setor.codsetor";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.preparedStatement.setString(1, cpf);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                func = new Funcionario();
                func.setCodfun(resultSet.getInt("codfun"));
                func.setNomfun(resultSet.getString("nomfun"));
                func.setCpffun(resultSet.getString("cpffun"));
                func.setSenha(resultSet.getString("senha"));
                func.setSetor(new Setor(resultSet.getInt("codsetor"), resultSet.getString("nomsetor")));
                func.setPermissao(new Permissao(resultSet.getInt("id_permissao"), resultSet.getString("nome_permissao")));
            }

        } catch (PSQLException e){
            e.printStackTrace();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return func;
    }
}