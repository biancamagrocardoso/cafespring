package br.csi.cafeterianova.dao;

import br.csi.cafeterianova.model.Cliente;
import br.csi.cafeterianova.model.Funcionario;
import br.csi.cafeterianova.model.LogPedido;
import br.csi.cafeterianova.model.Pedido;
import org.apache.commons.logging.Log;
import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LogPedDao {
    private String sql;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    public ArrayList<LogPedido> listPed(){
        ArrayList<LogPedido> pedidos = new ArrayList<LogPedido>();

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = " SELECT * FROM pedido";
            this.preparedStatement = connection.prepareStatement(this.sql);
            this.resultSet = this.preparedStatement.executeQuery();
            while (resultSet.next()){
                LogPedido ped = new LogPedido();
                Cliente cliente = new ClienteDao().getCli(resultSet.getInt("codcli"));
                ped.setCliente(cliente);
                ped.setDescricao(resultSet.getString("descricao"));
                ped.setMesa(resultSet.getInt("mesa"));
                Funcionario func = new FuncionarioDao().getFuncionario(resultSet.getInt("codfun"));
                ped.setFuncionario(func);
                pedidos.add(ped);
            }

        }catch (SQLException e){
            e.printStackTrace();
        }

        return pedidos;
    }

    public void setPedido(LogPedido ped){

        try (Connection connection = new ConectaDB().getConexao()){
            this.sql = "INSERT INTO pedido (descricao, mesa, codfun, codcli) VALUES (?, ?, ?, ?)";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, ped.getDescricao());
            preparedStatement.setInt(2, ped.getMesa());
            preparedStatement.setInt(3, ped.getFuncionario().getCodfun());
            preparedStatement.setInt(4, ped.getCliente().getCodcli());
            preparedStatement.execute();

        }catch (PSQLException e){
            e.printStackTrace();
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}
