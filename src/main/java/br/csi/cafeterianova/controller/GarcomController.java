package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.dao.PedidoDao;
import br.csi.cafeterianova.model.Login;
import br.csi.cafeterianova.model.Pedido;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@RequestMapping("/garcom")
public class GarcomController {

    @GetMapping("/inicio")
    public String inicio(Model model){

        model.addAttribute("login", new Login());

        model.addAttribute("peds", new PedidoDao().listPed());

        return "garcInicio";
    }

    @GetMapping("/comanda")
    public String comanda(@RequestParam int cpf, Model model){

        model.addAttribute("mesa", cpf);

        model.addAttribute("ped", new PedidoDao().getPed(cpf));

        return "comanda";
    }

    @PostMapping("/salvar")
    public RedirectView salvar(@ModelAttribute Pedido ped){

        if(new PedidoDao().getPed(ped.getMesa()).getNome() == null ){

            new PedidoDao().setPedido(ped);
        } else{

            new PedidoDao().updPed(ped);
        }

        return new RedirectView("/Cafeteria/garcom/inicio");
    }


}