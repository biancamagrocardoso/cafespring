<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page isELIgnored="false" %>
<html>
<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>Comanda</title>
</head>
<body>
<div class="container">
<h1>Mesa ${mesa}</h1>
<form:form action="/Cafeteria/garcom/salvar" method="post" modelAttribute="ped">
  <form:input path="mesa" type="hidden" value="${mesa}" />
  <form:input path="funcionario.codfun" type="hidden" value="${usuario_logado.codfun}" />

  <form:label path="nome">Cliente: </form:label>
  <form:input path="nome" type="text" />

  <form:label path="descricao">Descrição: </form:label>
  <form:input path="descricao" type="text" />

  <input type="submit" value="Salvar">
</form:form>
<h5><a href="/Cafeteria/garcom/inicio">Voltar!</a> </h5>
</div>
</body>
</html>
