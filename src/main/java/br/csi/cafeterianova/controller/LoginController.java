package br.csi.cafeterianova.controller;

import br.csi.cafeterianova.model.Funcionario;
import br.csi.cafeterianova.model.Login;
import br.csi.cafeterianova.service.LoginService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class LoginController {
    @GetMapping("login")
    public String logRed(Model model){
        model.addAttribute("login", new Login());

        return "login";
    }

    @PostMapping("login")
    public RedirectView login(@RequestParam String cpf, @RequestParam String senha, HttpServletRequest req){

        RedirectView redirect = new RedirectView("/Cafeteria/login");
        Funcionario func = new LoginService().autentica(cpf, senha);
        if (func != null){

            HttpSession sessao = req.getSession();
            sessao.setAttribute("usuario_logado", func);

            if (func.getSetor().getCodsetor() == 1){
                redirect = new RedirectView("/Cafeteria/admin/inicio");

            } else if (func.getSetor().getCodsetor() == 2) {
                redirect = new RedirectView("/Cafeteria/caixa/inicio");

            } else if (func.getSetor().getCodsetor() == 3) {
                redirect = new RedirectView("/Cafeteria/garcom/inicio");
            }
        }
        return redirect;
    }
}
